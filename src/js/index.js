let input = document.querySelector('input[type="text"]')
let form = document.querySelector("form")
let warning = document.querySelector(".warning")

form.addEventListener("submit", (e) => {
    e.preventDefault()

    if (input.value === "" || !input.value.includes("@")) {
        form.classList.add("invalid")
        warning.style.visibility = "visible"

        input.value = ""
    } else {
        form.classList.remove("invalid")
        warning.style.visibility = "hidden"
    }
})
